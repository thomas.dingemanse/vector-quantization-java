interface IPoint {

	void learn(Point p, double inputLearningRate, double outputLearningRate);

	double getDistance(Point p);

	double[] getInputs();

	double[] getOutputs();

	String toString();
}
