import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class VectorQuantization {

	private static BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

	private static int readInt() {
		try {
			return Integer.parseInt(br.readLine());
		} catch (IOException e) {
			System.out.println(e.getMessage());
			return 0;
		}
	}

	private static double readDouble() {
		try {
			return Double.parseDouble(br.readLine());
		} catch (IOException e) {
			System.out.println(e.getMessage());
			return 0;
		}
	}

	private static Point addPoint(int nInputs, int nOutputs) {
		double[] inputs = new double[nInputs];
		double[] outputs = new double[nOutputs];

		for(int j = 0; j < nInputs; j++) {
			System.out.print("\tEnter a value for input coordinate x" + j + ": ");
			inputs[j] = readDouble();
		}

		for(int j = 0; j < nOutputs; j++) {
			System.out.print("\tEnter a value for output value y" + j + ": ");
			outputs[j] = readDouble();
		}

		return new Point(inputs, outputs);
	}

	private static Point getNearestCluster(Point p, Point[] clusters) {
		int k = 0;
		double min = clusters[k].getDistance(p);

		for(int i = 0; i < clusters.length; i++) {
			double distance = clusters[i].getDistance(p);
			if(distance < min) {
				min = distance;
				k = i;
			}
		}

		System.out.println("Adjusting cluster " + k + "...");

		return clusters[k];
	}

	public static void main(String args[]) {
		System.out.print("Enter the desired number of clusters: ");
		int nClusters = readInt();

		System.out.print("Enter the desired number of data points: ");
		int nPoints = readInt();

		System.out.print("Enter the desired number of input coordinates per point: ");
		int nInputs = readInt();

		System.out.print("Enter the desired number of output values per point: ");
		int nOutputs = readInt();

		System.out.print("Enter the unsupervised learning rate gamma (0 < gamma < 1): ");
		double gamma = readDouble();

		System.out.print("Enter the unsupervised learning rate alpha (0 < alpha < 1): ");
		double alpha = readDouble();

		Point[] clusters = new Point[nClusters];
		Point[] points = new Point[nPoints];

		for(int i = 0; i < nClusters; i++) {
			System.out.println("Cluster " + i + ":");

			clusters[i] = addPoint(nInputs, nOutputs);
		}

		for(int i = 0; i < nPoints; i++) {
			System.out.println("Data point " + i + ":");

			points[i] = addPoint(nInputs, nOutputs);

			Point k = getNearestCluster(points[i], clusters);

			k.learn(points[i], gamma, alpha);
		}

		System.out.println("Final cluster data:");

		for(int i = 0; i < clusters.length; i++) {
			System.out.println("Cluster " + i + ":");
			System.out.println(clusters[i].toString());
		}
	}
}
