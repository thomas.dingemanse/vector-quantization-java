import java.text.DecimalFormat;

public class Point implements IPoint {

	private double[] inputs, outputs;

	Point(double[] inputs, double[] outputs) {
		this.inputs = inputs;
		this.outputs = outputs;
	}

	private void adjustVector(double[] v, double[] w, double learningRate) {

		if(v.length != w.length) {
			throw new IllegalArgumentException("The number of inputs/outputs of this data point does not equal the number of inputs/outputs of the point that should be updated.");
		}

		for(int i = 0; i < w.length; i++) {
			w[i] += learningRate * (v[i] - w[i]);
		}
	}

	public void learn(Point p, double inputLearningRate, double outputLearningRate) {

		/* Unsupervised learning (input coordinates). */
		adjustVector(p.getInputs(), this.inputs, inputLearningRate);

		/* Supervised learning (output values). */
		adjustVector(p.getOutputs(), this.outputs, outputLearningRate);
	}

	public double getDistance(Point p) {
		double distance = 0;

		for(int i = 0; i < p.inputs.length; i++) {
			distance += Math.pow(this.inputs[i] - p.inputs[i], 2);
		}

		return Math.sqrt(distance);
	}

	public double[] getInputs() {
		return inputs;
	}

	public double[] getOutputs() {
		return outputs;
	}

	@Override
	public String toString() {
		DecimalFormat df = new DecimalFormat("#.#####");

		StringBuilder sb = new StringBuilder("\tInputs: (");

		for(int i = 0; i < this.inputs.length; i++) {
			sb.append(df.format(inputs[i]));
			if(i < this.inputs.length - 1) {
				sb.append(", ");
			}
		}

		sb.append(").\n\tOutputs: (");

		for(int i = 0; i < this.outputs.length; i++) {
			sb.append(df.format(this.outputs[i]));
			if(i < this.outputs.length - 1) {
				sb.append(", ");
			}
		}

		sb.append(").");

		return sb.toString();
	}
}
